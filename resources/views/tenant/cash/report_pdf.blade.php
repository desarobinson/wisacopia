@php

$establishment = $cash->user->establishment;

$final_balance = 0;
$cash_income = 0;
$cash_egress = 0;
$cash_final_balance = 0;


$cash_documents = $cash->cash_documents;



foreach ($cash_documents as $cash_document) {

    //$final_balance += ($cash_document->document) ? $cash_document->document->total : $cash_document->sale_note->total;

    if($cash_document->sale_note){

        if($cash_document->sale_note->currency_type_id == 'PEN'){

            if(in_array($cash_document->sale_note->state_type_id, ['01','03','05','07','13'])){

                $cash_income += $cash_document->sale_note->total;
                $final_balance += $cash_document->sale_note->total;

            }


        }else{

            if(in_array($cash_document->sale_note->state_type_id, ['01','03','05','07','13'])){

                $cash_income += $cash_document->sale_note->total * $cash_document->sale_note->exchange_rate_sale;
                $final_balance += $cash_document->sale_note->total * $cash_document->sale_note->exchange_rate_sale;

            }

        }

        if(in_array($cash_document->sale_note->state_type_id, ['01','03','05','07','13'])){

            if( count($cash_document->sale_note->payments) > 0)
            {
                $pays = $cash_document->sale_note->payments;

                foreach ($methods_payment as $record) {

                    $record->sum = ($record->sum + $pays->where('payment_method_type_id', $record->id)->sum('payment') );
                }

            }
        }


    }
    else if($cash_document->document){

        if($cash_document->document->currency_type_id == 'PEN'){

            if(in_array($cash_document->document->state_type_id, ['01','03','05','07','13'])){

                $cash_income += $cash_document->document->total;
                $final_balance += $cash_document->document->total;

            }

        }else{

            if(in_array($cash_document->document->state_type_id, ['01','03','05','07','13'])){

                $cash_income += $cash_document->document->total * $cash_document->document->exchange_rate_sale;
                $final_balance += $cash_document->document->total * $cash_document->document->exchange_rate_sale;

            }

        }

        if(in_array($cash_document->document->state_type_id, ['01','03','05','07','13'])){

            if( count($cash_document->document->payments) > 0)
            {
                $pays = $cash_document->document->payments;

                foreach ($methods_payment as $record) {
                    // dd($pays, $record);

                    $record->sum = ($record->sum + $pays->where('payment_method_type_id', $record->id)->whereIn('document.state_type_id', ['01','03','05','07','13'])->sum('payment'));

                }

            }
        }




    }
    else if($cash_document->expense_payment){

        if($cash_document->expense_payment->expense->state_type_id == '05'){

            if($cash_document->expense_payment->expense->currency_type_id == 'PEN'){

                $cash_egress += $cash_document->expense_payment->payment;
                $final_balance -= $cash_document->expense_payment->payment;

            }else{

                $cash_egress += $cash_document->expense_payment->payment  * $cash_document->expense_payment->expense->exchange_rate_sale;
                $final_balance -= $cash_document->expense_payment->payment  * $cash_document->expense_payment->expense->exchange_rate_sale;
            }

        }
    }

}

$cash_final_balance = $final_balance + $cash->beginning_balance;
//$cash_income = ($final_balance > 0) ? ($cash_final_balance - $cash->beginning_balance) : 0;

@endphp
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="application/pdf; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Reporte POS - {{$cash->user->name}} - {{$cash->date_opening}} {{$cash->time_opening}}</title>
        <style>
            html {
                font-family: sans-serif;
                font-size: 12px;
            }

            table {
                width:33%;
                border-spacing: 0;
                border: 1px solid black;
            }

            .celda {
               
                padding: 5px;
                border: 0.1px solid black;
            }

            th {
                padding: 5px;
                
                border-color: #0088cc;
                border: 0.1px solid black;
            }

            .title {
                font-weight: bold;
                padding: 5px;
                font-size: 20px !important;
                text-decoration: underline;
            }

            p>strong {
                margin-left: 5px;
                font-size: 12px;
            }

            thead {
                font-weight: bold;
                background: #0088cc;
                color: white;
                text-align: center;
            }
            .td-custom { line-height: 0.1em; }

            .width-custom { width: 50% }
        </style>
    </head>
    <body>
        <div>
            <p  class="title"><strong>Reporte Punto de Venta</strong></p>
        </div>
        <div >


           
                        <p><strong>Empresa: </strong>{{$company->name}}</p>
                        <p><strong>Fecha reporte: </strong>{{date('Y-m-d')}}</p>
                        <p><strong>Ruc: </strong>{{$company->number}}</p>
                        <p><strong>Establecimiento: </strong>{{$establishment->address}} - {{$establishment->department->description}} - {{$establishment->district->description}}</p>
                        <p><strong>Vendedor: </strong>{{$cash->user->name}}</p>
                        <p><strong>Fecha y hora apertura: </strong>{{$cash->date_opening}} {{$cash->time_opening}}</p>
                        <p><strong>Estado de caja: </strong>{{($cash->state) ? 'Aperturada':'Cerrada'}}</p>
                        @if(!$cash->state)
                    
                        <p><strong>Fecha y hora cierre: </strong>{{$cash->date_closed}} {{$cash->time_closed}}</p>
                    
                    @endif
                    <p><strong>Montos de operación: </strong></p>
                    <p><strong>Saldo inicial: </strong>S/. {{number_format($cash->beginning_balance, 2, ".", "")}}</p>
                    <p><strong>Ingreso: </strong>S/. {{number_format($cash_income, 2, ".", "")}} </p>
                    <p><strong>Saldo final: </strong>S/. {{number_format($cash_final_balance, 2, ".", "")}} </p>
                    <p><strong>Egreso: </strong>S/. {{number_format($cash_egress, 2, ".", "")}} </p>
                  
        </div>
        @if($cash_documents->count())
            <div class="">
                <div class=" ">

                    <table>

                        <thead>
                            <tr>
                                
                                <th>Descripcion</th>
                              

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($methods_payment as $item)

                                <tr>
                                 
                                    <p></p>
                                    <td >{{ $item->name }} <strong> {{ number_format($item->sum, 2, ".", "")  }}</strong></td>

                                </tr>

                            @endforeach
                        </tbody>

                    </table> <br>

                    
                </div>
            </div>
        @else
            <div class="callout callout-info">
                <p>No se encontraron registros.</p>
            </div>
        @endif
    </body>
</html>
